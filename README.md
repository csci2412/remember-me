# Exercise 7: Adding login cookie

# Setup
 - clone this repository `git clone ...`
 - checkout the exercise branch `git fetch origin exercise-7 && git checkout exercise-7`
 - rename `config.ini.example` to `config.ini`
 - create a directory somewhere to hold your mysql credentials (e.g. `C:\etc`)
 - copy `db-params.json` to the directory created in the previous step and modify the credentials as necessary
 - set the correct path in `config.json` for `etc_directory` value
 - run `rememberme.sql` script in MySQL Workbench