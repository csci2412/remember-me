<?php include 'shared/header.php' ?>
<form action="login-submit.php" method="post">
	<fieldset>
		<legend>Log in to learn the secrets</legend>
		<div class="input-wrapper">
			<label for="username">Username:</label>
			<input type="text" id="username" name="username" maxlength="255" />
		</div>
		<div class="input-wrapper">
			<label for="password">Password:</label>
			<input type="password" id="password" name="password" />
		</div>
		<div class="input-wrapper">
			<label for="remember">Remember me:</label>
			<input type="checkbox" id="remember" name="remember" />
		</div>
		<button type="submit">Submit</button>
	</fieldset>
</form>
<?php include 'shared/footer.php' ?>