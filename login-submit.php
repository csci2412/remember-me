<?php 
include "shared/init.php";

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
	$username = $_POST['username'];
	$password = $_POST['password'];
	$remember = isset($_POST['remember']);

	login($db, $username, $password, $remember);
}