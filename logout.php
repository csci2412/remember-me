<?php
require_once 'shared/init.php';

// clear session
$_SESSION = [];

if (isset($_COOKIE['remember_me'])) {
	list($id) = parseCookie();

	$db->conn->query("delete from tokens where id = $id");

	// delete cookie
	setcookie('remember_me', '', time() - 3600);
}

header('Location: index.php');