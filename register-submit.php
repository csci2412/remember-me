<?php 
include "shared/init.php";

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
	$username = $_POST['username'];
	$password = $_POST['password'];
	$confirm = $_POST['confirm'];

	registerUser($db, $username, $password, $confirm);
}