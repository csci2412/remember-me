<?php include 'shared/header.php' ?>

<form action="register-submit.php" method="post">
	<fieldset>
		<legend>Create account</legend>
		<div class="input-wrapper">
			<label for="username">Username:</label>
			<input type="text" id="username" name="username" maxlength="255" />
		</div>
		<div class="input-wrapper">
			<label for="password">Password:</label>
			<input type="password" id="password" name="password" />
		</div>
		<div class="input-wrapper">
			<label for="confirm">Confirm Password:</label>
			<input type="password" id="confirm" name="confirm" />
		</div>
		<button type="submit">Submit</button>
	</fieldset>
</form>

<?php include 'shared/footer.php' ?>