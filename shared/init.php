<?php 
// init page with things we need like common functions, db connection, session
session_start();
include "functions.php";
$db = getDbHelper(getConfig());

// check if logged in based on cookie
tryRemember($db);